genrule(
    name = "gnucobol",
    visibility = ["//visibility:public"],
    srcs = glob(["**/*"]),
    outs = [":gnucobol.tar"],
    cmd = """
        set -o errexit
        PREFIX="$$PWD/build"
        mkdir "$$PREFIX"

        (
            cd 'external/gnucobol/gnucobol-2.2'
            ./configure                                             \\
                --prefix="$$PREFIX"                                 \\
                --disable-nls                                       \\
                --disable-shared                                    \\
                --with-pic                                          \\
                --without-curses
            make -j4
            make install
        )

        tar --create --file "$@" --directory "$$PREFIX" .
    """,
)
