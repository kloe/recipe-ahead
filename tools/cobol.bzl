def _cobol_binary(ctx):
    gnucobol = ctx.files.gnucobol[0]
    srcs = ctx.files.srcs
    out = ctx.outputs.out
    linkopts = ctx.attr.linkopts
    ctx.actions.run_shell(
        inputs = [gnucobol] + srcs,
        outputs = [out],
        command = """
            set -o errexit
            tar --extract --file "$1"
            gcc -o "$2" "${@:3}" -ldb -ldl -lgmp -Llib -l:libcob.a
        """,
        arguments =
            [gnucobol.path, out.path] +
            [src.path for src in srcs] +
            linkopts,
    )

def _cobol_library(ctx):
    gnucobol = ctx.files.gnucobol[0]
    srcs = ctx.files.srcs
    executable = ctx.attr.executable
    a = ctx.outputs.a

    objs = []

    for src in srcs:
        obj = ctx.actions.declare_file(
            "_objs/" + ctx.label.name + "/" + src.path[:-len(src.extension)] + "o",
            sibling = src,
        )
        ctx.actions.run_shell(
            inputs = [gnucobol] + srcs,
            outputs = [obj],
            command = """
                set -o errexit

                # cobc writes temporary files without namespacing, introducing
                # race conditions when building in parallel. Luckily, it reads
                # the TMPDIR environment variable.
                export TMPDIR="$PWD/cobc-tmp"
                mkdir "$TMPDIR"

                tar --extract --file "$1"
                bin/cobc                                            \\
                    --conf share/gnucobol/config/default.conf       \\
                    -I include                                      \\
                    $2 -c -o "$3" "$4"
            """,
            arguments = [gnucobol.path, "-x" if executable else "", obj.path, src.path],
        )
        objs.append(obj)

    ctx.actions.run(
        inputs = objs,
        outputs = [a],
        executable = "ar",
        arguments = ["crs", a.path] + [obj.path for obj in objs],
    )

cobol_binary = rule(
    implementation = _cobol_binary,
    attrs = {
        "gnucobol": attr.label(
            allow_single_file = True,
            default = "@gnucobol//:gnucobol.tar",
        ),
        "srcs": attr.label_list(allow_files = True),
        "linkopts": attr.string_list(),
    },
    outputs = {
        "out": "%{name}.out",
    },
)

cobol_library = rule(
    implementation = _cobol_library,
    attrs = {
        "gnucobol": attr.label(
            allow_single_file = True,
            default = "@gnucobol//:gnucobol.tar",
        ),
        "srcs": attr.label_list(allow_files = True),
        "executable": attr.bool(default = False),
    },
    outputs = {
        "a": "%{name}.a",
    },
)
