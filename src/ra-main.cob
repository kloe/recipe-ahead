       IDENTIFICATION DIVISION.
       PROGRAM-ID. ra-main.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 ws-email-status              PIC XX.
       01 ws-email-smtp-url            PIC X(100).
       01 ws-email-recipient           PIC X(100).
       01 ws-email-payload             PIC X(1000).

       PROCEDURE DIVISION.
           MOVE "john@example.com" TO ws-email-recipient

           STRING "To: john@example.com" X"0D0A"
                  "Subject: Shopping list" X"0D0A"
                  X"0D0A"
                  "Hello, world!" X"0D0A"
           INTO ws-email-payload

           ACCEPT ws-email-smtp-url FROM COMMAND-LINE
           CALL STATIC "ra-send-email"
               USING ws-email-status
                     ws-email-smtp-url
                     ws-email-recipient
                     ws-email-payload
           DISPLAY ws-email-status
           STOP RUN
           .
