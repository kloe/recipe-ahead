       IDENTIFICATION DIVISION.
       PROGRAM-ID. ra-send-email.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
      ******************************************************************
      * Constants defined by libcurl. TODO: Write a C program that     *
      * includes the appropriate header and generates a copybook.      *
      ******************************************************************
       01 ws-curl-opt-verbose          PIC 9(5) BINARY VALUE    41.
       01 ws-curl-opt-upload           PIC 9(5) BINARY VALUE    46.
       01 ws-curl-opt-url              PIC 9(5) BINARY VALUE 10002.
       01 ws-curl-opt-readdata         PIC 9(5) BINARY VALUE 10009.
       01 ws-curl-opt-mail-rcpt        PIC 9(5) BINARY VALUE 10187.

      ******************************************************************
      * libcurl easy handle and temporary state used for error         *
      * handling.                                                      *
      ******************************************************************
       01 ws-curl                      POINTER.
       01 ws-curl-status               PIC 9(5) BINARY.

      ******************************************************************
      * libcurl options to be set.                                     *
      ******************************************************************
       01 ws-curl-verbose              PIC 9(10) BINARY.
       01 ws-curl-url                  PIC X(101).
       01 ws-curl-mail-rcpt            POINTER.
       01 ws-curl-readdata             POINTER.
       01 ws-curl-upload               PIC 9(10) BINARY.

      ******************************************************************
      * The data to send to the SMTP server. From this, a file handle  *
      * (a la FILE*) is created using fmemopen, which is stored inside *
      * the ws-curl-readdata data item.                                *
      ******************************************************************
       01 ws-recipient-z.
           02 ws-recipient             PIC X(100).
           02 FILLER                   PIC X VALUE X"00".
       01 ws-payload                   PIC X(1000).

       LINKAGE SECTION.
      ******************************************************************
      * The status. The first letter indicates success or failure. The *
      * failure cases correspond to the various paragraphs in this     *
      * program. This should only be set from 0000-entry.              *
      ******************************************************************
       01 ls-status                    PIC XX.
           88 ls-status-initialize     VALUE "XI".
           88 ls-status-send           VALUE "XS".
           88 ls-status-finalize       VALUE "OF".

      ******************************************************************
      * The SMTP URL of the mail server to send the email to.          *
      ******************************************************************
       01 ls-smtp-url                  PIC X(100).

      ******************************************************************
      * The actual email.                                              *
      ******************************************************************
       01 ls-recipient                 PIC X(100).
       01 ls-payload                   PIC X ANY LENGTH.

       PROCEDURE DIVISION
           USING ls-status
                 ls-smtp-url
                 ls-recipient
                 ls-payload.
       0000-entry.
           SET ls-status-initialize TO TRUE
           PERFORM 1000-initialize

           SET ls-status-send TO TRUE
           PERFORM 2000-send

           SET ls-status-finalize TO TRUE
           GO TO 3000-finalize
           .

      ******************************************************************
      * Initialize the libcurl session and SMTP commands.              *
      ******************************************************************
       1000-initialize.
           PERFORM 1100-curl
           PERFORM 1200-curl-verbose
           PERFORM 1300-curl-url
           PERFORM 1400-curl-mail-rcpt
           PERFORM 1500-curl-readdata
           PERFORM 1600-curl-upload
           .

       1100-curl.
           CALL STATIC "curl_easy_init" GIVING ws-curl
           PERFORM 9999-check-curl
           .

       1200-curl-verbose.
           MOVE 1 TO ws-curl-verbose
           CALL STATIC "curl_easy_setopt"
               USING VALUE ws-curl
                     VALUE ws-curl-opt-verbose
                     VALUE ws-curl-verbose
               GIVING ws-curl-status
           PERFORM 9999-check-curl-status
           .

       1300-curl-url.
           STRING ls-smtp-url DELIMITED BY SPACE, X"00"
               INTO ws-curl-url
           CALL STATIC "curl_easy_setopt"
               USING VALUE     ws-curl
                     VALUE     ws-curl-opt-url
                     REFERENCE ws-curl-url
               GIVING ws-curl-status
           PERFORM 9999-check-curl-status
           .

       1400-curl-mail-rcpt.
           MOVE ls-recipient TO ws-recipient
           CALL STATIC "curl_slist_append"
               USING VALUE     ws-curl-mail-rcpt
                     REFERENCE ws-recipient-z
               GIVING ws-curl-mail-rcpt

           CALL STATIC "curl_easy_setopt"
               USING VALUE ws-curl
                     VALUE ws-curl-opt-mail-rcpt
                     VALUE ws-curl-mail-rcpt
               GIVING ws-curl-status
           PERFORM 9999-check-curl-status
           .

       1500-curl-readdata.
      * See note "Dynamic calls".
           CALL "fmemopen"
               USING REFERENCE ls-payload
                     VALUE     LENGTH OF ls-payload
                     REFERENCE "r" X"00"
               GIVING ws-curl-readdata
           PERFORM 9999-check-fmemopen

           CALL STATIC "curl_easy_setopt"
               USING VALUE ws-curl
                     VALUE ws-curl-opt-readdata
                     VALUE ws-curl-readdata
               GIVING ws-curl-status
           PERFORM 9999-check-curl-status
           .

       1600-curl-upload.
           MOVE 1 TO ws-curl-upload
           CALL STATIC "curl_easy_setopt"
               USING VALUE ws-curl
                     VALUE ws-curl-opt-upload
                     VALUE ws-curl-upload
               GIVING ws-curl-status
           PERFORM 9999-check-curl-status
           .

      ******************************************************************
      * Execute the previously initialized SMTP commands.              *
      ******************************************************************
       2000-send.
           CALL STATIC "curl_easy_perform"
               USING VALUE ws-curl
               GIVING ws-curl-status
           PERFORM 9999-check-curl-status
           .

      ******************************************************************
      * Clean up garbage and exit program. Jumped to from many places  *
      * as an early exit mechanism in case of errors.                  *
      ******************************************************************
       3000-finalize.
           IF ws-curl-readdata IS NOT EQUAL TO NULL THEN
      * See note "Dynamic calls".
               CALL "fclose"
                   USING VALUE ws-curl-readdata
               INITIALIZE ws-curl-readdata
           END-IF

           IF ws-curl-mail-rcpt IS NOT EQUAL TO NULL THEN
               CALL STATIC "curl_slist_free_all"
                   USING VALUE ws-curl-mail-rcpt
               INITIALIZE ws-curl-mail-rcpt
           END-IF

           IF ws-curl IS NOT EQUAL TO NULL THEN
               CALL STATIC "curl_easy_cleanup" USING VALUE ws-curl
               INITIALIZE ws-curl
           END-IF

           EXIT PROGRAM
           .

       9999-check-curl.
           IF ws-curl IS EQUAL TO NULL THEN
               GO TO 3000-finalize
           END-IF
           .

       9999-check-curl-status.
           IF ws-curl-status IS NOT EQUAL TO 0 THEN
               GO TO 3000-finalize
           END-IF
           .

       9999-check-fmemopen.
           IF ws-curl-readdata IS EQUAL TO NULL THEN
               GO TO 3000-finalize
           END-IF
           .
